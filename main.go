package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/alex.tran1502/MarsuBot/db"
	"gitlab.com/alex.tran1502/MarsuBot/discord"
)

func main() {
	dbInstance := db.Connect()
	defer dbInstance.Close()

	db.CreateSchema(dbInstance)

	dg, err := discordgo.New("Bot " + db.GoDotEnvVariable("DISCORD_BOT_KEY"))

	if err != nil {
		fmt.Println("Cannot connect to bot: ", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(discord.MessageHandler)

	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	log.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}
