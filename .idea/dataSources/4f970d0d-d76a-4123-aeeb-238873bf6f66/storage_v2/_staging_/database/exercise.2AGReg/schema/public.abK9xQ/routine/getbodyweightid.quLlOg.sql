create function getbodyweightid() returns integer
    language plpgsql
as
$$

BEGIN
    return query (select bw.id as bwid
    from body_weights bw
    where date_part('day', bw.timestamp) between date_part('day', now()) and date_part('day', now() + '1 day'::interval));
END;
$$;

alter function getbodyweightid() owner to alex;

