package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/joho/godotenv"
	"gitlab.com/alex.tran1502/MarsuBot/model"
)

var PgInstance *pg.DB

func GoDotEnvVariable(key string) string {

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}

// Connect open an instance to the database
func Connect() *pg.DB {
	godotenv.Load()

	dbName := GoDotEnvVariable("PG_DATABASE")
	dbPassword := GoDotEnvVariable("PG_PASSWORD")
	dbUsername := GoDotEnvVariable("PG_USERNAME")
	dbHostname := GoDotEnvVariable("PG_HOSTNAME")

	connectionString := fmt.Sprintf("postgresql://%s:%s@%s:5432/%s?sslmode=require", dbUsername, dbPassword, dbHostname, dbName)

	opt, err := pg.ParseURL(connectionString)
	if err != nil {
		panic(err)
	}

	PgInstance = pg.Connect(opt)

	ctx := context.Background()

	if err := PgInstance.Ping(ctx); err != nil {
		panic(err)
	}

	// var value map[string]interface{}
	// db.Query(&value, "select * from discord")
	// fmt.Println(value["exercise_name"])

	return PgInstance
}

// CreateSchema Create tables
func CreateSchema(db *pg.DB) error {

	opts := &orm.CreateTableOptions{
		IfNotExists: true,
		Varchar:     256,
	}

	models := []interface{}{
		(*model.Exercise)(nil),
		(*model.BodyWeight)(nil),
		(*model.MuscleGroup)(nil),
		(*model.ExerciseList)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(opts)

		if err != nil {
			return err
		}

	}

	log.Println("Create Table Success!")
	return nil

}
