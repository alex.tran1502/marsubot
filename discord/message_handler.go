package discord

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/alex.tran1502/MarsuBot/db"
	"gitlab.com/alex.tran1502/MarsuBot/model"
)

// MessageHandler Handler endpoint for all routes
func MessageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	// List Exercise
	if strings.HasPrefix(m.Content, "!l") {
		muscleGroup := strings.Split(m.Content, " ")
		if len(muscleGroup) != 2 {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Invalid Command\nShould be !l <muscle_group | all>)")
			return
		}
		exercises := BuildExerciseList(muscleGroup[1])
		_, _ = s.ChannelMessageSendComplex(m.ChannelID, &discordgo.MessageSend{
			Content: exercises,
		})
	}

	if strings.HasPrefix(m.Content, "!w") {

		tokens := strings.Split(m.Content, " ")

		if len(tokens) != 2 {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Invalid Command\nShould be !w <body_weight>")
			return
		}

		bodyWeight, _ := strconv.ParseFloat(tokens[1], 32)

		todayWeight := &model.BodyWeight{
			BodyWeight: float32(bodyWeight),
		}

		_, err := db.PgInstance.Model(todayWeight).Insert()
		if err != nil {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Add Today Weight Failed")
			return
		}
		_, _ = s.ChannelMessageSend(m.ChannelID, "Today Weight Added")
	}

	// Command Syntax: !u <chest>,<chest_exercise_id>,<set>,<weight>,<rep>
	if strings.HasPrefix(m.Content, "!u") {
		tokens := strings.Split(m.Content, " ")

		tokenInfo := strings.Split(tokens[1], ",")

		if len(tokenInfo) != 4 {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Invalid Command\nShould be !u <exercise_name>,<set>,<weight_level>,<repetition>")
			return
		}

		insertedExercise, err := AddExercise(tokenInfo)

		if err != nil {
			_, _ = s.ChannelMessageSend(m.ChannelID, "Failed to add exercise")
		}

		_, _ = s.ChannelMessageSend(m.ChannelID, "Added "+insertedExercise+" To DB")
	}
}
