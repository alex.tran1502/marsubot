package discord

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/alex.tran1502/MarsuBot/db"
	"gitlab.com/alex.tran1502/MarsuBot/model"
)

// AddExercise Add exercise to its group in DB
func AddExercise(exerciseInfo []string) (string, error) {

	exerciseID, _ := strconv.ParseInt(exerciseInfo[0], 10, 32)
	set, _ := strconv.ParseInt(exerciseInfo[1], 10, 32)
	weight, _ := strconv.ParseFloat(exerciseInfo[2], 32)
	rep, _ := strconv.ParseInt(exerciseInfo[3], 10, 32)

	exercise := &model.Exercise{
		ExerciseId: int32(exerciseID),
		Set:        int32(set),
		Weight:     float32(weight),
		Rep:        int32(rep),
	}

	_, err := db.PgInstance.Model(exercise).Insert()

	if err != nil {
		log.Println("Failed to add discord", err)
		return "", err
	}

	var exerciseName map[string]interface{}
	db.PgInstance.Query(&exerciseName, `
		select el.description as desc
		from exercise_list el
		where el.id = ?
	`, int32(exerciseID))

	insertedExercise := fmt.Sprintf("%s", exerciseName["desc"])

	return insertedExercise, nil
}

// BuildExerciseList Show a list of exercise based on muscle group and its ID
func BuildExerciseList(muscleGroup string) string {
	muscleGroup = strings.ToLower(muscleGroup)

	if matched, _ := regexp.MatchString(`chest`, muscleGroup); matched == true {
		return queryExercises("chest")
	} else if matched, _ := regexp.MatchString(`back`, muscleGroup); matched == true {
		return queryExercises("back")
	} else if matched, _ := regexp.MatchString(`arm`, muscleGroup); matched == true {
		return queryExercises("arms")
	} else if matched, _ := regexp.MatchString(`shoulder`, muscleGroup); matched == true {
		return queryExercises("shoulders")
	} else if matched, _ := regexp.MatchString(`abs`, muscleGroup); matched == true {
		return queryExercises("abs")
	} else if matched, _ := regexp.MatchString(`leg`, muscleGroup); matched == true {
		return queryExercises("legs")
	} else {
		fmt.Println("Get All Exercise")
		var exerciseResult []map[string]interface{}
		result := ""

		db.PgInstance.Query(&exerciseResult, `
			SELECT el.id, el.description as desc
			FROM exercise_list el;
		`)
		for _, exercise := range exerciseResult {
			result += fmt.Sprintf("[%d] %s\n", exercise["id"], exercise["desc"])
		}

		return result
	}
}

func queryExercises(muscleGroup string) string {
	result := ""
	var exerciseResult []map[string]interface{}

	db.PgInstance.Query(&exerciseResult, `
			select el.id, el.description as desc
			from exercise_list el
					 left outer join muscle_group mg on mg.id = el.muscle_group_id
			where mg.name = ?
		`, muscleGroup)

	for _, exercise := range exerciseResult {
		result += fmt.Sprintf("[%d] %s\n", exercise["id"], exercise["desc"])
	}

	return result
}
