package model

import (
	"time"
)

// Exercise discord struct
type Exercise struct {
	ID           int32     `pg:"id,pk"`
	ExerciseId   int32     `pg:"exercise_id"`
	Set          int32     `pg:"set,notnull"`
	Weight       float32   `pg:"weight,notnull"`
	Rep          int32     `pg:"rep,notnull"`
	BodyWeightID int32     `pg:"body_weight_id,default:getbodyweightid()"`
	Timestamp    time.Time `pg:"timestamp,default:now()"`
}

// BodyWeight daily body weight with timestamp
type BodyWeight struct {
	ID         int32     `pg:"id,pk"`
	BodyWeight float32   `pg:"body_weight,notnull"`
	Timestamp  time.Time `pg:"timestamp,default:now()"`
}

// MuscleGroup
type MuscleGroup struct {
	tableName struct{} `pg:"muscle_group"`
	ID        int32    `pg:"id,pk"`
	Name      string   `pg:"name,notnull"`
}

// Exerciselist
type ExerciseList struct {
	tableName     struct{} `pg:"exercise_list"`
	ID            int32    `pg:"id,pk"`
	MuscleGroupId int32    `pg:"muscle_group_id,notnull"`
	Description   string   `pg:"description,notnull"`
}
