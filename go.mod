module gitlab.com/alex.tran1502/MarsuBot

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/go-pg/pg/v10 v10.6.1
	github.com/joho/godotenv v1.3.0
)
